﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Number_of_lines
{
    //coucou
    public partial class Form1 : Form
    {
        char[] tabChars = { ' ', '\t', '\0' };
        string[] ListeFiles;
        string[] ListeFolder;
        long TotalLines = 0;
        public Form1()
        {
            InitializeComponent();
        }
        public void LireDossier(string path,string extension)
        {
            
            ListeFolder = System.IO.Directory.GetDirectories(path);
            if (ListeFolder != null || ListeFolder.Length > 0)
            {
                foreach (string dossier in ListeFolder)
                {
                    LireDossier(dossier, extension);
                }
            }
            ListeFiles = Directory.GetFiles(path, extension, SearchOption.TopDirectoryOnly);
            foreach (string file in ListeFiles)
            {
                string[] ligne = { file, NumberofLines(file).ToString() };
                Dgv.Rows.Add(ligne);
                TotalLines = TotalLines + NumberofLines(file);
            }

        }


        public long NumberofLines(string path) //retourne le nombre de lignes d'un fichier texte, en utilisant le fichier ouvert en paramètre.
        {
            long nbLines = 0;
            System.IO.StreamReader sr = new System.IO.StreamReader(path);
            string s = sr.ReadLine();
            string[] lines = File.ReadAllLines(path);
            foreach (string ligne in lines)
            {
                //if (ligne.Trim(tabChars) == "") { nbLines = nbLines; }
                //if (ligne.Trim(tabChars).StartsWith("//")) nbLines = nbLines;
                if (!((ligne.Trim(tabChars) == ""))) nbLines++; //Si la ligne n'est pas vide ou n'est pas un commentaire,
                // on incrémente le compteur
            }
            return nbLines;
        }



        private void b_browse_Click(object sender, EventArgs e) // Code du bouton parcourir
        {
            int uncheck = 0;
            if (checkBoxc.Checked)
            {
                uncheck++;
            }
            if (checkBoxcpp.Checked)
            {
                uncheck++;
            }
            if (checkBoxh.Checked)
            {
                uncheck++;
            }
            if (checkBoxvb.Checked)
            {
                uncheck++;
            }
            if (checkBoxaspx.Checked)
            {
                uncheck++;
            }
            if (checkBoxhtm.Checked)
            {
                uncheck++;
            }
            if (checkBoxhtml.Checked)
            {
                uncheck++;
            }
            if (checkBoxmaster.Checked)
            {
                uncheck++;
            }
            if (checkBoxcss.Checked)
            {
                uncheck++;
            }
            if (checkBoxjs.Checked)
            {
                uncheck++;
            }
            if (checkBoxconfig.Checked)
            {
                uncheck++;
            }
            if (checkBoxctrl.Checked)
            {
                uncheck++;
            }
            if (checkBoxcsproj.Checked)
            {
                uncheck++;
            }
            if (checkBoxcs.Checked)
            {
                uncheck++;
            }
            if (uncheck == 0) MessageBox.Show("Aucune extension choisie !");
            else
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.RootFolder = System.Environment.SpecialFolder.MyComputer;
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    Dgv.ColumnCount = 2;
                    string chemin = fbd.SelectedPath;
                    if (checkBoxc.Checked)
                    {
                        LireDossier(chemin, "*.c");
                        uncheck++;
                    }
                    if (checkBoxcpp.Checked)
                    {
                        LireDossier(chemin, "*.cpp");
                        uncheck++;
                    }
                    if (checkBoxmaster.Checked)
                    {
                        LireDossier(chemin, "*.master");
                        uncheck++;
                    }
                    if (checkBoxconfig.Checked)
                    {
                        LireDossier(chemin, "*.config");
                        uncheck++;
                    }
                    if (checkBoxh.Checked)
                    {
                        LireDossier(chemin, "*.h");
                        uncheck++;
                    }
                    if (checkBoxvb.Checked)
                    {
                        LireDossier(chemin, "*.vb");
                        uncheck++;
                    }
                    if (checkBoxaspx.Checked)
                    {
                        LireDossier(chemin, "*.asp?");
                        uncheck++;
                    }
                    if (checkBoxhtm.Checked)
                    {
                        LireDossier(chemin, "*.htm");
                        uncheck++;
                    }
                    if (checkBoxhtml.Checked)
                    {
                        LireDossier(chemin, "*.html");
                        uncheck++;
                    }
                    if (checkBoxcss.Checked)
                    {
                        LireDossier(chemin, "*.css");
                        uncheck++;
                    }
                    if (checkBoxjs.Checked)
                    {
                        LireDossier(chemin, "*.js");
                        uncheck++;
                    }
                    if (checkBoxctrl.Checked)
                    {
                        LireDossier(chemin, "*.ctrl");
                        uncheck++;
                    }
                    if (checkBoxcsproj.Checked)
                    {
                        LireDossier(chemin, "*.csproj");
                        uncheck++;
                    }
                    if (checkBoxcs.Checked)
                    {
                        LireDossier(chemin, "*.cs");
                        uncheck++;
                    }
                }
            }
            if (uncheck != 0)
            {
                string[] lignetotal = { "TOTAL", TotalLines.ToString() };
                Dgv.Rows.Add(lignetotal);
            }
        }
    }
}
