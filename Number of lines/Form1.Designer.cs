﻿namespace Number_of_lines
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_browse = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.checkBoxc = new System.Windows.Forms.CheckBox();
            this.checkBoxh = new System.Windows.Forms.CheckBox();
            this.checkBoxcpp = new System.Windows.Forms.CheckBox();
            this.checkBoxvb = new System.Windows.Forms.CheckBox();
            this.checkBoxaspx = new System.Windows.Forms.CheckBox();
            this.checkBoxhtm = new System.Windows.Forms.CheckBox();
            this.checkBoxhtml = new System.Windows.Forms.CheckBox();
            this.checkBoxcss = new System.Windows.Forms.CheckBox();
            this.checkBoxjs = new System.Windows.Forms.CheckBox();
            this.checkBoxctrl = new System.Windows.Forms.CheckBox();
            this.checkBoxcsproj = new System.Windows.Forms.CheckBox();
            this.checkBoxcs = new System.Windows.Forms.CheckBox();
            this.checkBoxmaster = new System.Windows.Forms.CheckBox();
            this.checkBoxconfig = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // b_browse
            // 
            this.b_browse.Location = new System.Drawing.Point(12, 190);
            this.b_browse.Name = "b_browse";
            this.b_browse.Size = new System.Drawing.Size(172, 23);
            this.b_browse.TabIndex = 0;
            this.b_browse.Text = "Parcourir...";
            this.b_browse.UseVisualStyleBackColor = true;
            this.b_browse.Click += new System.EventHandler(this.b_browse_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Dgv
            // 
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(212, 12);
            this.Dgv.Name = "Dgv";
            this.Dgv.Size = new System.Drawing.Size(579, 161);
            this.Dgv.TabIndex = 4;
            // 
            // checkBoxc
            // 
            this.checkBoxc.AutoSize = true;
            this.checkBoxc.Location = new System.Drawing.Point(12, 12);
            this.checkBoxc.Name = "checkBoxc";
            this.checkBoxc.Size = new System.Drawing.Size(35, 17);
            this.checkBoxc.TabIndex = 5;
            this.checkBoxc.Text = ".c";
            this.checkBoxc.UseVisualStyleBackColor = true;
            // 
            // checkBoxh
            // 
            this.checkBoxh.AutoSize = true;
            this.checkBoxh.Location = new System.Drawing.Point(12, 35);
            this.checkBoxh.Name = "checkBoxh";
            this.checkBoxh.Size = new System.Drawing.Size(35, 17);
            this.checkBoxh.TabIndex = 6;
            this.checkBoxh.Text = ".h";
            this.checkBoxh.UseVisualStyleBackColor = true;
            // 
            // checkBoxcpp
            // 
            this.checkBoxcpp.AutoSize = true;
            this.checkBoxcpp.Location = new System.Drawing.Point(12, 58);
            this.checkBoxcpp.Name = "checkBoxcpp";
            this.checkBoxcpp.Size = new System.Drawing.Size(47, 17);
            this.checkBoxcpp.TabIndex = 7;
            this.checkBoxcpp.Text = ".cpp";
            this.checkBoxcpp.UseVisualStyleBackColor = true;
            // 
            // checkBoxvb
            // 
            this.checkBoxvb.AutoSize = true;
            this.checkBoxvb.Location = new System.Drawing.Point(12, 81);
            this.checkBoxvb.Name = "checkBoxvb";
            this.checkBoxvb.Size = new System.Drawing.Size(41, 17);
            this.checkBoxvb.TabIndex = 8;
            this.checkBoxvb.Text = ".vb";
            this.checkBoxvb.UseVisualStyleBackColor = true;
            // 
            // checkBoxaspx
            // 
            this.checkBoxaspx.AutoSize = true;
            this.checkBoxaspx.Location = new System.Drawing.Point(12, 104);
            this.checkBoxaspx.Name = "checkBoxaspx";
            this.checkBoxaspx.Size = new System.Drawing.Size(52, 17);
            this.checkBoxaspx.TabIndex = 9;
            this.checkBoxaspx.Text = ".asp?";
            this.checkBoxaspx.UseVisualStyleBackColor = true;
            // 
            // checkBoxhtm
            // 
            this.checkBoxhtm.AutoSize = true;
            this.checkBoxhtm.Location = new System.Drawing.Point(12, 127);
            this.checkBoxhtm.Name = "checkBoxhtm";
            this.checkBoxhtm.Size = new System.Drawing.Size(46, 17);
            this.checkBoxhtm.TabIndex = 10;
            this.checkBoxhtm.Text = ".htm";
            this.checkBoxhtm.UseVisualStyleBackColor = true;
            // 
            // checkBoxhtml
            // 
            this.checkBoxhtml.AutoSize = true;
            this.checkBoxhtml.Location = new System.Drawing.Point(98, 12);
            this.checkBoxhtml.Name = "checkBoxhtml";
            this.checkBoxhtml.Size = new System.Drawing.Size(48, 17);
            this.checkBoxhtml.TabIndex = 11;
            this.checkBoxhtml.Text = ".html";
            this.checkBoxhtml.UseVisualStyleBackColor = true;
            // 
            // checkBoxcss
            // 
            this.checkBoxcss.AutoSize = true;
            this.checkBoxcss.Location = new System.Drawing.Point(98, 35);
            this.checkBoxcss.Name = "checkBoxcss";
            this.checkBoxcss.Size = new System.Drawing.Size(45, 17);
            this.checkBoxcss.TabIndex = 12;
            this.checkBoxcss.Text = ".css";
            this.checkBoxcss.UseVisualStyleBackColor = true;
            // 
            // checkBoxjs
            // 
            this.checkBoxjs.AutoSize = true;
            this.checkBoxjs.Location = new System.Drawing.Point(98, 58);
            this.checkBoxjs.Name = "checkBoxjs";
            this.checkBoxjs.Size = new System.Drawing.Size(36, 17);
            this.checkBoxjs.TabIndex = 13;
            this.checkBoxjs.Text = ".js";
            this.checkBoxjs.UseVisualStyleBackColor = true;
            // 
            // checkBoxctrl
            // 
            this.checkBoxctrl.AutoSize = true;
            this.checkBoxctrl.Location = new System.Drawing.Point(98, 81);
            this.checkBoxctrl.Name = "checkBoxctrl";
            this.checkBoxctrl.Size = new System.Drawing.Size(43, 17);
            this.checkBoxctrl.TabIndex = 14;
            this.checkBoxctrl.Text = ".ctrl";
            this.checkBoxctrl.UseVisualStyleBackColor = true;
            // 
            // checkBoxcsproj
            // 
            this.checkBoxcsproj.AutoSize = true;
            this.checkBoxcsproj.Location = new System.Drawing.Point(98, 104);
            this.checkBoxcsproj.Name = "checkBoxcsproj";
            this.checkBoxcsproj.Size = new System.Drawing.Size(57, 17);
            this.checkBoxcsproj.TabIndex = 15;
            this.checkBoxcsproj.Text = ".csproj";
            this.checkBoxcsproj.UseVisualStyleBackColor = true;
            // 
            // checkBoxcs
            // 
            this.checkBoxcs.AutoSize = true;
            this.checkBoxcs.Location = new System.Drawing.Point(98, 127);
            this.checkBoxcs.Name = "checkBoxcs";
            this.checkBoxcs.Size = new System.Drawing.Size(40, 17);
            this.checkBoxcs.TabIndex = 16;
            this.checkBoxcs.Text = ".cs";
            this.checkBoxcs.UseVisualStyleBackColor = true;
            // 
            // checkBoxmaster
            // 
            this.checkBoxmaster.AutoSize = true;
            this.checkBoxmaster.Location = new System.Drawing.Point(12, 151);
            this.checkBoxmaster.Name = "checkBoxmaster";
            this.checkBoxmaster.Size = new System.Drawing.Size(60, 17);
            this.checkBoxmaster.TabIndex = 17;
            this.checkBoxmaster.Text = ".master";
            this.checkBoxmaster.UseVisualStyleBackColor = true;
            // 
            // checkBoxconfig
            // 
            this.checkBoxconfig.AutoSize = true;
            this.checkBoxconfig.Location = new System.Drawing.Point(98, 151);
            this.checkBoxconfig.Name = "checkBoxconfig";
            this.checkBoxconfig.Size = new System.Drawing.Size(58, 17);
            this.checkBoxconfig.TabIndex = 18;
            this.checkBoxconfig.Text = ".config";
            this.checkBoxconfig.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 225);
            this.Controls.Add(this.checkBoxconfig);
            this.Controls.Add(this.checkBoxmaster);
            this.Controls.Add(this.checkBoxcs);
            this.Controls.Add(this.checkBoxcsproj);
            this.Controls.Add(this.checkBoxctrl);
            this.Controls.Add(this.checkBoxjs);
            this.Controls.Add(this.checkBoxcss);
            this.Controls.Add(this.checkBoxhtml);
            this.Controls.Add(this.checkBoxhtm);
            this.Controls.Add(this.checkBoxaspx);
            this.Controls.Add(this.checkBoxvb);
            this.Controls.Add(this.checkBoxcpp);
            this.Controls.Add(this.checkBoxh);
            this.Controls.Add(this.checkBoxc);
            this.Controls.Add(this.Dgv);
            this.Controls.Add(this.b_browse);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_browse;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.CheckBox checkBoxc;
        private System.Windows.Forms.CheckBox checkBoxh;
        private System.Windows.Forms.CheckBox checkBoxcpp;
        private System.Windows.Forms.CheckBox checkBoxvb;
        private System.Windows.Forms.CheckBox checkBoxaspx;
        private System.Windows.Forms.CheckBox checkBoxhtm;
        private System.Windows.Forms.CheckBox checkBoxhtml;
        private System.Windows.Forms.CheckBox checkBoxcss;
        private System.Windows.Forms.CheckBox checkBoxjs;
        private System.Windows.Forms.CheckBox checkBoxctrl;
        private System.Windows.Forms.CheckBox checkBoxcsproj;
        private System.Windows.Forms.CheckBox checkBoxcs;
        private System.Windows.Forms.CheckBox checkBoxmaster;
        private System.Windows.Forms.CheckBox checkBoxconfig;
    }
}

